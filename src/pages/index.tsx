import {
  Text
} from '@chakra-ui/react'
import { Container } from '../components/Container'

const Index = () => (
  <Container height="100vh">
    <Text>Hello World</Text>
  </Container>
)

export default Index
