import React, { useContext, useState, useEffect, createContext } from 'react'
import nookies from 'nookies'
import authTypes from '@firebase/auth-types/'
import firebase from 'firebase'
import firebaseClient from '../firebase/firebaseClient'

const AuthContext = createContext(null)

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  firebaseClient()
  const [currentAuthUser, setCurrentAuthUser] = useState<authTypes.User>(null)
  const [loading, setLoading] = useState<Boolean>(true)

  function signup(email, password) {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
  }
  
  function login(email, password) {
    return firebase.auth().signInWithEmailAndPassword(email, password)
  }
  
  function signOut() {
    return firebase.auth().signOut()
  }
  
  function resetPassword(email) {
    return firebase.auth().sendPasswordResetEmail(email)
  }
  
  useEffect(() => {
    const unsubscribe = firebase.auth().onIdTokenChanged(async user => {
      if (!user) {
        nookies.set(undefined, 'token', '', {})
        setCurrentAuthUser(null)
      } else {
        const token = await user.getIdToken()
        nookies.set(undefined, 'token', token, {})
        setCurrentAuthUser(user)
      }
      setLoading(false)
    })
    return unsubscribe
  }, [])
  
  const value = {
    currentAuthUser,
    signup,
    login,
    signOut,
    resetPassword
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
