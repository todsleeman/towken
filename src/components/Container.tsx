import { Flex } from '@chakra-ui/react'

export const Container = (props) => {

  const bgColor = 'gray.50'

  const color = 'black'

  return (
    <Flex
      direction="column"
      alignItems="center"
      justifyContent="flex-start"
      bg={bgColor}
      color={color}
      {...props}
    />
  )
}
